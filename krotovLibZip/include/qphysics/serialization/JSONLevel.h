//
//  JSONLevel.h
//  qphysics
//
//  Created by Donatas Tutinas on 27/04/15.
//  Copyright (c) 2015 Donatas Tutinas. All rights reserved.
//

#ifndef __qphysics__JSONLevel__
#define __qphysics__JSONLevel__

#include <stdio.h>
#include "../Level.h"

struct SerializedPotential
{
public:
    std::string id;
    std::string expression;
    
    bool operator==(const SerializedPotential& given) const
    {
        if (given.id != this->id)
            return false;
        if (given.expression != this->expression)
            return false;
        
        return true;
    }

};

struct SerializedLevel
{
public:
    std::string id;
    std::string name;
    double kineticFactor;
    int numberOfGridpoints;
    std::vector<SerializedPotential> potentials;
    std::string dynamicPotential;
    std::string staticPotential;
    std::string targetWavefunction;
    std::string initialWavefunction;
    std::vector<double> targetArea;

	Conditions conditions;
    std::string divX = "";
    std::string divAmp = "";
    
    bool operator==(const SerializedLevel& given) const
    {
        if (given.id != this->id)
            return false;
        if (given.name != this->name)
            return false;
        if (given.kineticFactor != this->kineticFactor)
            return false;
        if (given.numberOfGridpoints != this->numberOfGridpoints)
            return false;
        if (given.dynamicPotential != this->dynamicPotential)
            return false;
        if (given.staticPotential != this->staticPotential)
            return false;
        if (given.targetWavefunction != this->targetWavefunction)
            return false;
        if (given.initialWavefunction != this->initialWavefunction)
            return false;
        
        if (given.potentials != this->potentials)
            return false;
        if (given.targetArea != this->targetArea)
            return false;
        
        if (given.divX != this->divX)
            return false;
        if (given.divAmp != this->divAmp)
            return false;
        
        return true;
    }
};

class JSONLevel
{
public:
    /// <summary>
    /// Loads a levels from a JSON string
    /// </summary>
    /// <param name="json file path"></param>
    /// <returns></returns>
    static Level* loadLevel(std::string jsonPath, bool twiceSpatPts = false);
    
    static SerializedLevel deserialize(std::string jsonPath);
    
    //caches serialized level in the local storage and returns
    //the name of the file
    static std::string cacheLevel(std::string jsonLevel, std::string levelId);
    
private:
    /// <summary>
    /// Creates a ParsedPotential from a string. If the string contains variables that is defined in
    /// the list of potentials, i.e. "p1", it is substituted into the string
    /// </summary>
    /// <param name="p">The expression string from the json-file</param>
    /// <param name="potentials">A list of expressions with correspinding id's from the json-file</param>
    /// <returns></returns>
    static PotentialPtr createPot(std::string p, std::vector<SerializedPotential> potentials);

	static TargetAreaPtr createTargetArea(SerializedLevel level);

	/// <summary>
	/// Creates a StateBuilder from a wavefunction string. The string should be formatted as
	/// potentialExpression1[number,number,...] + potentialExpression2[number,number,...] + ...
	/// The algorithm is then: 
	///   1) Find the next occurence of [ and from that deduce "potentialExpression1". Use this
	///      to create the potential using the createPot(...) method. That means one can both use
	///      explicit notation (exp(x-x0)) or use predefined ones (p1) or combinations (p1+Pow(x-x0,2)).
	///   2) It then finds the next occurence of ] and parse what is in between as an array of complex numbers.
	///   3) It then adds the potential and complex numbers to the StateBuilder.
	///   4) Afterwards it finds the "+" between and cuts awway the first part and do it all over until there are
	///      no more.
	/// </summary>
	/// <param name="wavefunctionString"></param>
	/// <param name="potentials"></param>
	/// <returns></returns>
	static StateBuilderPtr createStateBuilder(std::string wavefunctionString, std::vector<SerializedPotential> potentials);
};

#endif /* defined(__qphysics__JSONLevel__) */
