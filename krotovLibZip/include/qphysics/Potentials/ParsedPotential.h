//
//  ParsedPotential.h
//  qphysics
//
//  Created by Donatas Tutinas on 29/04/15.
//  Copyright (c) 2015 Donatas Tutinas. All rights reserved.
//

#ifndef __qphysics__ParsedPotential__
#define __qphysics__ParsedPotential__

#include <stdio.h>
#include "Potential.h"
#include "CustomParser/Parser.h"
#include "symbolic/symbolicc++.h"

class ParsedPotential : public Potential
{
public:
    ParsedPotential(std::string exp);
    arma::vec getDiscretePotential(arma::vec& xArray, double t);
    arma::vec getDiscreteDivPos(arma::vec& xArray, double t);
    arma::vec getDiscreteDivAmp(arma::vec& xArray, double t);

	std::string getDivXExpr() {
		return parserDivPos.getExpr();
	}

	std::string getDivAmpExpr() {
		return parserDivAmp.getExpr();
	}
    
    
private:
    bool initedDivX = false;
    bool initedDivAmp = false;
    
    void setCustomDivX(const std::string& expr)
    {
        parserDivPos.setExpr(expr);
        initedDivX = true;
    }
    
    void setCustomDivAmp(const std::string& expr)
    {
        parserDivAmp.setExpr(expr);
        initedDivAmp = true;
    }
    
    void initDivPot();
    vp::Parser parser;
    vp::Parser parserDivPos;
    vp::Parser parserDivAmp;
    Symbolic* iterate(vp::ExpressionNode* node);
};

#endif /* defined(__qphysics__ParsedPotential__) */
