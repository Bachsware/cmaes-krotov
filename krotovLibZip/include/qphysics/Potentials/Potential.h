//
//  Potential.h
//  qphysics
//
//  Created by Donatas Tutinas on 31/03/15.
//  Copyright (c) 2015 Donatas Tutinas. All rights reserved.
//

#ifndef __qphysics__Potential__
#define __qphysics__Potential__

#include "../Types.h"
#include <armadillo>

/// <summary>
/// The class can be overriden to specify a potential. It supports basic overloaded aritmetic, +,-,*,/.
/// </summary>
class Potential
{
private:
    bool isMoveable = true;
public:
    virtual ~Potential();
    
    /// <summary>
    /// Returns whether this potential will react to updatePotential-method.
    /// </summary>
    /// <returns></returns>
    virtual bool moveable()
    {
        return isMoveable;
    }
    
    /// <summary>
    /// Sets whether this potential will be recentered or not. It can thus be made
    /// inactive by setting FASLE
    /// </summary>
    /// <param name="isMoveable"></param>
    virtual void setMoveable(bool isMoveable)
    {
        this->isMoveable = isMoveable;
    }
    
    /// <summary>
    /// Resets the internal state of a potential
    /// </summary>
    virtual void reset()
    {
        
    }
    
	/// <summary>
	/// Updates the potential to be evaluated with a new extremum
	/// </summary>
	/// <param name="p">A Point containing the new extremum positon and amplitude</param>
	virtual void updatePotential(Point p)
	{
		currentPoint = p;
	}
    
    virtual void setCustomDivX(const std::string& expr)
    {
        
    }
    
    virtual void setCustomDivAmp(const std::string& expr)
    {
        
    }

	/// <summary>
	/// Calculates the potential evaluated at a specific x
	/// </summary>
	/// <param name="xArray">The position array at which the derivatives are evaluated</param>
	/// <returns>Array of Potential derivatives</returns>
	virtual arma::vec getDiscretePotential(arma::vec& xArray, double t) = 0;

	/// <summary>
	/// Calculates the derivative of the potential with respect to the ekstremum position evaluated at a specific x position
	/// </summary>
	/// <param name="xArray">The position array at which the derivatives are evaluated</param>
	/// <returns>Array of Potential derivatives</returns>
	virtual arma::vec getDiscreteDivPos(arma::vec& xArray, double t) = 0;

	/// <summary>
	/// Calculates the derivative of the potential with respect to the ekstremum amplitude evaluated at a specific x position
	/// </summary>
	/// <param name="xArray">The position array at which the derivatives are evaluated</param>
	/// <returns>Array of Potential derivatives</returns>
	virtual arma::vec getDiscreteDivAmp(arma::vec& xArray, double t) = 0;

	Potential* operator+(Potential& sp);
	Potential* operator-(Potential& sp);
	Potential* operator*(Potential& sp);
	Potential* operator/(Potential& sp);

protected:
	Point currentPoint = Point(0, 0, 0);
};

/// <summary>
/// A potential that is a sum of to potentials. Used in the overloaded + operator.
/// </summary>
class SumPotential : public Potential
{
private:
	Potential *f, *g;
public:
	SumPotential(Potential* f, Potential* g) : f((f)), g((g)) {}

	~SumPotential() {
		delete f;
		delete g;
	}

	arma::vec getDiscretePotential(arma::vec& xArray, double t)
	{
		return f->getDiscretePotential(xArray, t) + g->getDiscretePotential(xArray, t);
	}

	arma::vec getDiscreteDivPos(arma::vec& xArray, double t)
	{
		return f->getDiscreteDivPos(xArray, t) + g->getDiscreteDivPos(xArray, t);
	}

	arma::vec getDiscreteDivAmp(arma::vec& xArray, double t)
	{
		return f->getDiscreteDivAmp(xArray, t) + g->getDiscreteDivAmp(xArray, t);
	}

	void updatePotential(Point p)
	{
		f->updatePotential(p);
		g->updatePotential(p);
	}
};

/// <summary>
/// A potential that is a difference of to potentials. Used in the overloaded - operator.
/// </summary>
class DiffPotential : public Potential
{
private:
	Potential *f, *g;
public:
	DiffPotential(Potential* f, Potential* g) : f((f)), g((g)) {}

	~DiffPotential() {
		delete f;
		delete g;
	}

	arma::vec getDiscretePotential(arma::vec& xArray, double t)
	{
		return f->getDiscretePotential(xArray, t) - g->getDiscretePotential(xArray, t);
	}

	arma::vec getDiscreteDivPos(arma::vec& xArray, double t)
	{
		return f->getDiscreteDivPos(xArray, t) - g->getDiscreteDivPos(xArray, t);
	}

	arma::vec getDiscreteDivAmp(arma::vec& xArray, double t)
	{
		return f->getDiscreteDivAmp(xArray, t) - g->getDiscreteDivAmp(xArray, t);
	}

	void updatePotential(Point p)
	{
		f->updatePotential(p);
		g->updatePotential(p);
	}
};

/// <summary>
/// A potential that is a multipla of to potentials. Used in the overloaded * operator.
/// </summary>
class MulPotential : public Potential
{
private:
	Potential *f, *g;
public:
	MulPotential(Potential* f, Potential* g) : f((f)), g((g)) {}

	~MulPotential() {
		delete f;
		delete g;
	}

	arma::vec getDiscretePotential(arma::vec& xArray, double t)
	{
		return f->getDiscretePotential(xArray, t) % g->getDiscretePotential(xArray, t);
	}

	arma::vec getDiscreteDivPos(arma::vec& xArray, double t)
	{
		return f->getDiscreteDivPos(xArray, t) % g->getDiscretePotential(xArray, t) + g->getDiscreteDivPos(xArray, t) % f->getDiscretePotential(xArray, t);
	}

	arma::vec getDiscreteDivAmp(arma::vec& xArray, double t)
	{
		return f->getDiscreteDivAmp(xArray, t) % g->getDiscretePotential(xArray, t) + g->getDiscreteDivAmp(xArray, t) % f->getDiscretePotential(xArray, t);
	}

	void updatePotential(Point p)
	{
		f->updatePotential(p);
		g->updatePotential(p);
	}
};

/// <summary>
/// A potential that is a division of to potentials. Used in the overloaded / operator.
/// </summary>
class DivPotential : public Potential
{
private:
	Potential *f, *g;
public:
	DivPotential(Potential* f, Potential* g) : f((f)), g((g)) {}

	~DivPotential() {
		delete f;
		delete g;
	}

	arma::vec getDiscretePotential(arma::vec& xArray, double t)
	{
		return f->getDiscretePotential(xArray, t) / g->getDiscretePotential(xArray, t);
	}

	arma::vec getDiscreteDivPos(arma::vec& xArray, double t)
	{
		return (f->getDiscreteDivPos(xArray, t) % g->getDiscretePotential(xArray, t) - g->getDiscreteDivPos(xArray, t) % f->getDiscretePotential(xArray, t))
			/ arma::pow(g->getDiscretePotential(xArray, t), 2);

	}

	arma::vec getDiscreteDivAmp(arma::vec& xArray, double t)
	{
		return (f->getDiscreteDivAmp(xArray, t) % g->getDiscretePotential(xArray, t) - g->getDiscreteDivAmp(xArray, t) % f->getDiscretePotential(xArray, t))
			/ arma::pow(g->getDiscretePotential(xArray, t), 2);
	}

	void updatePotential(Point p)
	{
		f->updatePotential(p);
		g->updatePotential(p);
	}
};

class NullPotential : public Potential
{
	arma::vec getDiscretePotential(arma::vec& xArray, double t)
	{
		return 0 * xArray;
	}

	arma::vec getDiscreteDivPos(arma::vec& xArray, double t)
	{
		return 0 * xArray;
	}

	arma::vec getDiscreteDivAmp(arma::vec& xArray, double t)
	{
		return 0 * xArray;
	}
};

#endif /* defined(__qphysics__Potential__) */
