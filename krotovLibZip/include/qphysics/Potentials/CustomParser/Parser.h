//
//  Parser.h
//  cppTest2
//
//  Created by Donatas Tutinas on 02/05/15.
//  Copyright (c) 2015 Donatas Tutinas. All rights reserved.
//

#ifndef __cppTest2__Parser__
#define __cppTest2__Parser__

#include <stdio.h>
#include "ExpressionNode.hpp"
namespace vp
{
    class Parser
    {
    public:
        arma::vec* x;
        double t, x0, amp;
        ExpressionNode* expNode = NULL;
        
        ~Parser();
        void setExpr(std::string str);
        std::string getExpr()
        {
            return expression;
        }
        arma::vec getValue(int length);
        
    private:
        std::string expression = "";
        ExpressionNode* parse(std::string s);
        ExpressionNode* ParseBasicExpression(std::string identifier);
        inline bool convertToDouble(const std::string& s, double* value)
        {
            std::istringstream i(s);
            double x;
            if (!(i >> x))
                return false;
            else {
                *value = x;
                return true;
            }
        }
        
        std::vector<std::string> splitOnFunctionComma(std::string str);
        
        ExpressionNode* initPowType(const std::string& value, double exponent);
        void failure(const std::string& excMsg);
        
        bool reachesNegativeParenthesis(const std::string& str);
        std::string removeBoilerplateParenthesis(std::string str);
        std::string parenthesizeSmallPow(std::string str);
    };
    
    class NotWellFormed : public std::exception {
    private:
        std::string message = "The expression is not valid";
        
    public:
        NotWellFormed() {}
        NotWellFormed(std::string message) : message(message)
        {
            
        }
        virtual const char* what() const throw()
        {
            return message.c_str();
        }
    };
}
#endif /* defined(__cppTest2__Parser__) */
