//
//  ExpressionNode.h
//  cppTest2
//
//  Created by Donatas Tutinas on 02/05/15.
//  Copyright (c) 2015 Donatas Tutinas. All rights reserved.
//

#ifndef __cppTest2__ExpressionNode__
#define __cppTest2__ExpressionNode__

#include <stdio.h>
#include <armadillo>

namespace vp
{
    
    class ExpressionNode
    {
    public:
        ExpressionNode *left = NULL, *right = NULL;
        
        virtual arma::vec getValue(int length) = 0;
        virtual ~ExpressionNode()
        {
            delete left;
            delete right;
        }
    };
    
    class ScalarBase : public ExpressionNode
    {
    public:
        virtual double getScalar() = 0;
        virtual arma::vec getValue(int length) = 0;
    };
    
    class Scalar : public ScalarBase
    {
    private:
        const double constant;
    public:
        Scalar(double constant) : constant(constant)
        {
            
        }
        
        double getScalar()
        {
            return constant;
        }
        
        arma::vec getValue(int length)
        {
            //std::cout << "Filling Scalar: " << constant << std::endl;
            arma::vec vec(length);
            return vec.fill(constant);
        }
    };
    
    class Variable : public ExpressionNode
    {
    private:
        const std::string varName;
    public:
        Variable(const std::string& varName) : varName(varName) {}
        
        std::string getVarName()
        {
            return varName;
        }
    };
    
    class ScalarVariable : public Variable
    {
    public:
        double &value;
        
        ScalarVariable(double &value, const std::string& varName) : value(value), Variable(varName)
        {
            
        }
        
        double getScalar()
        {
            return value;
        }
        
        arma::vec getValue(int length)
        {
            //std::cout << "Filling ScalarVariable: " << value << std::endl;
            arma::vec vec(length);
            return vec.fill(value);
        }
    };
    
    class VectorVariable : public Variable
    {
    public:
        arma::vec **value;
        
        VectorVariable(const std::string& varName) : Variable(varName)
        {
            
        }
        
        VectorVariable(arma::vec **vec, const std::string& varName) : Variable(varName)
        {
            value = vec;
        }
        
        arma::vec getValue(int length)
        {
            return **value;
        }
    };
    
    class Square : public ExpressionNode
    {
    public:
        Square() {}
        
        arma::vec getValue(int length)
        {
            
            return arma::pow(left->getValue(length), 2);
        }
    };
    
    class Pow : public ExpressionNode
    {
    private:
        const double exponent;
        
    public:
        Pow(double exponent) : exponent(exponent) {}
        
        double getExponent()
        {
            return exponent;
        }
        
        arma::vec getValue(int length)
        {
            return arma::pow(left->getValue(length), exponent);
        }
    };
    
    class Exp : public ExpressionNode
    {
        arma::vec getValue(int length)
        {
            return arma::exp(left->getValue(length));
        }
    };
    
    class Zero : public Scalar
    {
    public:
        Zero() : Scalar(0) {}
    };
    
    class Gauss : public ExpressionNode
    {
    private:
        VectorVariable* x;
        
    public:
        Gauss(ExpressionNode* mean, ExpressionNode* sigma, VectorVariable* x) : x(x)
        {
            left = mean;
            right = sigma;
        }
            
        arma::vec getValue(int length)
        {
            return arma::exp( -arma::pow((x->getValue(length) - left->getValue(length))/right->getValue(length), 2) / 2 );
        }
    };
    
    class Sum : public ExpressionNode
    {
        arma::vec getValue(int length)
        {
            if(ScalarBase* v = dynamic_cast<ScalarBase*>(left))
            {
                double leftVal = v->getScalar();
                arma::vec rightVal = right->getValue(length);
                return leftVal + rightVal;
            }
            if(ScalarBase* v = dynamic_cast<ScalarBase*>(right))
            {
                double rightVal = v->getScalar();
                arma::vec leftVal = left->getValue(length);
                return leftVal + rightVal;
            }
            
            return left->getValue(length) + right->getValue(length);
        }
    };
    
    class Diff : public ExpressionNode
    {
        arma::vec getValue(int length)
        {
            if(ScalarBase* v = dynamic_cast<ScalarBase*>(left))
            {
                double leftVal = v->getScalar();
                arma::vec rightVal = right->getValue(length);
                return leftVal - rightVal;
            }
            if(ScalarBase* v = dynamic_cast<ScalarBase*>(right))
            {
                double rightVal = v->getScalar();
                arma::vec leftVal = left->getValue(length);
                return leftVal - rightVal;
            }
            return left->getValue(length) - right->getValue(length);
        }
    };
    
    class Mul : public ExpressionNode
    {
        arma::vec getValue(int length)
        {
            if(ScalarBase* v = dynamic_cast<ScalarBase*>(left))
            {
                double leftVal = v->getScalar();
                arma::vec rightVal = right->getValue(length);
                return leftVal * rightVal;
            }
            if(ScalarBase* v = dynamic_cast<ScalarBase*>(right))
            {
                double rightVal = v->getScalar();
                arma::vec leftVal = left->getValue(length);
                return leftVal * rightVal;
            }
            return left->getValue(length) % right->getValue(length);
        }
    };
    
    class Div : public ExpressionNode
    {
        arma::vec getValue(int length)
        {
            if(ScalarBase* v = dynamic_cast<ScalarBase*>(left))
            {
                double leftVal = v->getScalar();
                arma::vec rightVal = right->getValue(length);
                return leftVal / rightVal;
            }
            if(ScalarBase* v = dynamic_cast<ScalarBase*>(right))
            {
                double rightVal = v->getScalar();
                arma::vec leftVal = left->getValue(length);
                return leftVal / rightVal;
            }
            return left->getValue(length) / right->getValue(length);
        }
    };
}
#endif /* defined(__cppTest2__ExpressionNode__) */
