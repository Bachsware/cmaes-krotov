//
//  TargetArea.h
//  qphysics
//
//  Created by Donatas Tutinas on 31/03/15.
//  Copyright (c) 2015 Donatas Tutinas. All rights reserved.
//

#ifndef __qphysics__TargetArea__
#define __qphysics__TargetArea__

#include "Types.h"

/// <summary>
/// This class encapsulates a square area inside which points can be checked or moved to.
/// This is used to describe the target area in a Level instance.
/// </summary>
class TargetArea
{
public:
	/// <summary>
	/// Constructs a square target area.
	/// </summary>
	/// <param name="x">The left lower x value of the target area</param>
	/// <param name="y">The left lower y value of the target area</param>
	/// <param name="width">The width of the target area</param>
	/// <param name="height">The height o the target area</param>
	TargetArea(double x, double y, double width, double height);

	virtual ~TargetArea();

	double width;
	double height;
	double x;
	double y;

	/// <summary>
	/// Checks whether a point is inside a target area
	/// </summary>
	/// <param name="newP">The point to be checked</param>
	/// <returns>True if the point is inside, false else</returns>
	bool inInside(Point newP);

	/// <summary>
	/// Checks if the point is inside the target area. If not a corresponding
	/// point moved into the target area is returned. Else a copy of the original point
	/// is returned.
	/// </summary>
	/// <param name="newP">The point to be checked.</param>
	/// <returns>A point that is inside the target area</returns>
	Point moveInto(Point newP);
};

#endif /* defined(__qphysics__TargetArea__) */
