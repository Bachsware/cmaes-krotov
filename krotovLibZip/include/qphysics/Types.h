/*
 Types used for qphysics
 */

#pragma once
#include <vector>
#include <complex>
#include <stdexcept>
#include <memory>

struct SmallPoint {
public:
	double x = 0, amp = 0;
	SmallPoint() {
	}
	SmallPoint(double x, double amp) :
		x(x), amp(amp) {
	}

	double& operator[](const unsigned int index) {
		switch (index) {
		case 0:
			return x;
		case 1:
			return amp;
		default:
			throw std::invalid_argument("Wrong index accessing SmallPoint"); // works with debug, but not release
		}
	}
};

struct Point : public SmallPoint{
	double dTime = 0;

	Point() :
		SmallPoint() {
	}
	Point(double x, double amp) :
		SmallPoint(x, amp) {
	}
	Point(double x, double amp, double dTime) :
		SmallPoint(x, amp), dTime(dTime) {
	}

	double& operator[](const unsigned int index) {
		switch (index) {
		case 0:
			return x;
		case 1:
			return amp;
		case 2:
			return dTime;
		default:
			throw std::invalid_argument("Wrong index accessing Point"); // works with debug, but not release
		}
	}

	friend bool operator==(const Point point, const Point point2) {
		return point.x == point2.x && point.amp == point2.amp && point.dTime == point.dTime;
	}

	friend bool operator!=(const Point point, const Point point2) {
		return point.x != point2.x || point.amp != point2.amp || point.dTime != point.dTime;
	}
};

struct RefPoint {
	double *x, *amp;

	RefPoint(double *x, double *amp) :
		x(x), amp(amp) {
	}

	double& operator[](const unsigned int index) {
		switch (index) {
		case 0:
			return *x;
		case 1:
			return *amp;
		default:
			throw std::invalid_argument("Wrong index accessing RefPoint"); // works with debug, but not release
		}
	}
};

//matlab specific types
struct PathPoint : public Point {
	double fidelity = 0;
	std::vector<std::complex<double>> psis;

	PathPoint() :
		Point() {
	}
	PathPoint(double fidelity) :
		fidelity(fidelity){
	}
	PathPoint(double x, double amp) :
		Point(x, amp) {
	}
	PathPoint(double x, double amp, double dTime) :
		Point(x, amp, dTime) {
	}
	PathPoint(double x, double amp, double dTime, double fidelity) :
		Point(x, amp, dTime), fidelity(fidelity) {
	}
};

class WaveFunction;
class Potential;
class TargetArea;
class StateBuilder;

typedef std::unique_ptr<WaveFunction> WaveFunctionPtr;
typedef std::unique_ptr<Potential> PotentialPtr;
typedef std::unique_ptr<TargetArea> TargetAreaPtr;
typedef std::unique_ptr<StateBuilder> StateBuilderPtr;


typedef std::vector<PathPoint> Path;


//end matlab specific types
