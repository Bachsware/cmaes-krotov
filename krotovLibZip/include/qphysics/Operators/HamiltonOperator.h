#pragma once
#include "HermitianOperator.h"

class HamiltonOperator :
	public HermitianOperator
{
public:
	HamiltonOperator();
	~HamiltonOperator();

	void applyI(WaveFunction& wave);
};