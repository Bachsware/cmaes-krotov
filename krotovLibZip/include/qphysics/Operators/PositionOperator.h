#pragma once
#include "HermitianOperator.h"

class PositionOperator :
	public HermitianOperator
{
public:
	PositionOperator();
	~PositionOperator();

	void applyI(WaveFunction& wave);
};

