#pragma once
#include "../Wavefunction/WaveFunction.h"

/// <summary>
/// This class represents a Hermitian operator, i.e. an operator with real expectation values. All that have to be done is 
/// overriding the method to apply the operator.
/// </summary>
class HermitianOperator
{
public:
	HermitianOperator();
	virtual ~HermitianOperator();
	/// <summary>
	/// Applies the operator inplace, i.e. changes the internal state of the wavefunction
	/// </summary>
	/// <param name="wave">The wavefunction to operate on</param>
	virtual void applyI(WaveFunction& wave) = 0;
	/// <summary>
	/// Applies the operator and returns a new wavefunction
	/// </summary>
	/// <param name="wave">The wavefunction to operate on</param>
	/// <returns></returns>
	WaveFunction apply(WaveFunction& wave);
	/// <summary>
	/// Calculates the variance of the operator 
	/// </summary>
	/// <param name="wave"></param>
	/// <returns></returns>
	double getVariance(WaveFunction& wave);
	/// <summary>
	/// Calculates the expectation value of the operator
	/// </summary>
	/// <param name="wave"></param>
	/// <returns></returns>
	double getExpectationValue(WaveFunction& wave);

protected:
	/// <summary>
	/// This applies the operator twice, and is used in the variance calculations. Override if you need to optimize
	/// the naive implementation.
	/// </summary>
	/// <param name="wavefunction">The wavefuncto to operate on twice</param>
	/// <returns></returns>
	virtual void applyTwiceI(WaveFunction& wavefunction);
};

