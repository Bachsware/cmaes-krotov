/*
Base class for other levels to inherit common
methods and members.
*/

#pragma once
#include <memory>
#include "Wavefunction/DynamicWaveFunction.h"
#include "HamiltonianSystem.h"
#include "Potentials/ParsedPotential.h"
#include "StateBuilder.h"
#include "TargetArea.h"

struct Conditions {
	// Starting parameters : [x0 amp]
	Point paramStart;

	// Constraints for the u parameters
	Point uMax;
	Point uMin;
	Point dudt;
    
    bool operator==(const Conditions& given) const
    {
        if (given.paramStart != this->paramStart)
            return false;
        if (given.uMax != this->uMax)
            return false;
        if (given.uMin != this->uMin)
            return false;
        if (given.dudt != this->dudt)
            return false;
        
        return true;
    }
};

/// <summary>
/// <para>
/// Level encaptulates all the properties of a quantum system. It can conveniently be used to 
/// propagate a quantum system defined by the level. </para>
/// <para></para>
/// <para>
/// All levels must derrive from this class.
/// The following functions must be overriden:<para></para>
/// <para>createTargetWaveFunction(Point p): Calculates the target state inside the defined target area</para>
/// <para>createInitialWaveFunction(): Creates the initial wavefunction of the system</para>
/// <para>createDynamicPotential(): Defines the potential that is updated in time and extremum</para>
/// <para>createStaticPotential(): Defines the potential that is not updated</para>
/// <para>createTargetArea(): Defines the target area used to determine where the target state is allowed</para>
/// </para>
/// </summary>
class Level
{
private:
	WaveFunctionPtr initialState;
	StateBuilderPtr targetStateBuilder;

public:
    std::vector<arma::vec> getParsedDivPot(Point point)
    {
        std::vector<arma::vec> divPotential(2);
        
        H->dynamicPotential->updatePotential(point);
        
        divPotential[0] = H->dynamicPotential->getDiscreteDivPos(H->xArray, point.dTime);
        divPotential[1] = H->dynamicPotential->getDiscreteDivAmp(H->xArray, point.dTime);
        return divPotential;
    }

	/// <summary>
	/// Constructor to initialize a Level object. This is usually used when extending the class.
	/// </summary>
	/// <param name="kinematicFactor">The kinetic factor of the system</param>
	/// <param name="nSpatialPts">The number of discrete points used in all calculations</param>
	/// <param name="startPoint">The start extremum point of the potential</param>
	/// <param name="xMinVal">The minimum possible x value</param>
	/// <param name="xMaxVal">The maximum possible x value</param>
	/// <param name="ampMinVal">The minimum possible amplitude (or y value)</param>
	/// <param name="ampMaxVal">The maximum possible amplitude (or y value)</param>
	Level(PotentialPtr dynamicPotential, PotentialPtr staticPotential, double kinematicFactor, int nSpatialPts, Conditions conditions, TargetAreaPtr targetArea, StateBuilderPtr targetStateBuilder, StateBuilderPtr initialStateBuilder, bool twiceSpatPts = false);

	TargetAreaPtr targetArea;

	// Starting conditions
	Conditions conditions;

	virtual ~Level();

	/// <summary>
	/// Updates the potential to the point and propagates the wavefunction. It makes sure the 
	/// time setps are consistent. USE THIS TO UPDATE THE SYSMTE INSTEAD OF
	/// USING THE HamiltonianSystem REFERENCE UNLESS YOU KNOW WHAT YOU ARE DOING.
	/// ! ALWAYS UPDATE PREVIOUS POINT BEFORE PROPOGATING TO THE POINT YOU START YOUR PROPOGATION !
	/// </summary>
	/// <param name="wave">The wave to be propagated in place</param>
	/// <param name="point">The new point, describing the new extremum and time step</param>
	/// <param name="forward">Specifies whether the wave is propagated forward(true) or backwards(false) in time</param>
	/// <param name="normalize">if normalize is true, normalization will be done explicit, else implicit</param>
	void updatePotentialAndPropagateWave(DynamicWaveFunction& wave, Point point, bool forward = true, bool normalize = true);

	/// <summary>
	/// Resets level to initial state.
	/// </summary>
	void reset();

	/// <summary>
	/// The initial starting point of the level
	/// </summary>
	/// <returns>The initial point</returns>
	Point getStartPoint()
	{
		return conditions.paramStart;
	}

	/// <summary>
	/// Returns a wavefunction with the initial state
	/// </summary>
	/// <returns>A wavefunction with the initial state copy</returns>
	WaveFunction getInitialWaveFunction()
	{
		return WaveFunction(*initialState);
	}

	/// <summary>
	/// Returns a reference of the initial state
	/// ONLY USE THIS IF YOU KNOW WHAT YOU ARE DOING. 
	/// </summary>
	/// <returns>A copy of the initial state</returns>
	arma::cx_vec getInitialState()
	{
		return initialState->returnState();
	}

	/// <summary>
	/// Returns the target state corresponding to the current state of the Hamiltonian
	/// Uses current point of Hamiltonian system
	/// </summary>
	/// <returns>The current target state</returns>
	WaveFunctionPtr getTargetWaveFunction()
	{
		return getTargetWaveFunction(H->getCurrentPoint());
	}

	/// <summary>
	/// Returns the target state corresponding to the given point
	/// </summary>
	/// <returns>The target state of the given point</returns>
	WaveFunctionPtr getTargetWaveFunction(Point point)
	{
		if (targetStateBuilder != NULL)
		{
			targetStateBuilder->updatePotentials(targetArea->moveInto(point));
			return targetStateBuilder->build(*H);
		}
		else
		{
			arma::cx_vec vec(H->nSpatialPts);
			vec.zeros();
			return WaveFunctionPtr(new WaveFunction(vec, *H));
		}
	}

	/// <summary>
	/// Returns reference of the Hamiltonian system. This can be usefull to calculate
	/// eigenstates of potentials that are not the given potential of the level.
	/// ONLY USE THIS TO UPDATE THE POTENTIAL AND PROPAGATE A WAVE IF
	/// YOU KNOW WHAT YOU ARE DOING. 
	/// </summary>
	/// <returns>The hamiltonian system</returns>
	HamiltonianSystem& getHamiltonian()
	{
		return *H;
	}

	/// <summary>
	/// Checks if the given point is in the level world.
	/// Moves it back in if it is not.
	/// </summary>
	void checkPointBounds(Point& point);

	/// <summary>
	/// Checks if the given point is in range from previous point.
	/// Moves it back in to meet range limit condition.
	/// </summary>
	void checkPointSpeed(Point& point);

protected:
	HamiltonianSystem* H;
};


