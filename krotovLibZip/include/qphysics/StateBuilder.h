#pragma once
#include "Types.h"
#include "HamiltonianSystem.h"

/// <summary>
/// Abstract class to create state from a sequence of eigenstates from different potentials.
/// </summary>
class StateBuilder
{
protected:
	std::vector<PotentialPtr> potentials;
	std::vector<arma::cx_vec> coefs;
public:
	StateBuilder();
	~StateBuilder();
	/// <summary>
	/// Add a potential from which the created state will contain components from the eigenspectrum
	/// of the supplied potential. The components will be scaled with complex coeffecients. The length
	/// of the coeffecient array determines the number of contributions added to the final state. 
	/// </summary>
	/// <param name="p">The potential form which eigenstates should be added</param>
	/// <param name="c">Coeffecients of the contributions</param>
	/// <returns>A reference to the current StateBuilder</returns>
	void addContribution(PotentialPtr p, std::vector<std::complex<double>> c);

	/// <summary>
	/// Adds the ground state of the potential to the final state
	/// </summary>
	/// <param name="p">The potential to calculated the ground state from</param>
	/// <returns>A reference to the current StateBuilder</returns>
	//StateBuilderPtr addContribution(PotentialPtr p);

	/// <summary>
	/// Updates all the potentials to the given point.
	/// </summary>
	/// <param name="p">The point</param>
	void updatePotentials(Point p);

	/// <summary>
	/// Builds a normalized wavefunction from all the given contributions, using a supplied
	/// HamiltonianSystem. 
	/// </summary>
	/// <param name="h">The HamiltonianSystem to create wavefunction with</param>
	/// <returns></returns>
	WaveFunctionPtr build(HamiltonianSystem& h);
};

