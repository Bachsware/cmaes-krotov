#pragma once
#include <fftw3.h>
#include "WaveFunction.h"

class DynamicWaveFunction : public WaveFunction
{
private:
	fftw_complex* dataPointer;
	fftw_plan p_fft;
	fftw_plan p_ifft; //Remember that ifft is not normalized  as in MATLAB
	arma::vec preCalc;

	void initFFTW();

public:
	DynamicWaveFunction(HamiltonianSystem& wave);
	DynamicWaveFunction(WaveFunction& wave);
	~DynamicWaveFunction();

	/// <summary>
	/// Propagates a wavefunction in place a timestep dt in the potential described by the HamiltonianSystem. When used in conjunction with
	/// updatePotential(Point p), make sure the timestep in the given point and the dt used in this method is consistent!!!
	/// </summary>
	/// <param name="dt">The timestep of the propagation</param>
	/// <param name="forward">Propagates forward if true, else backward</param>
	/// <param name="normalize">if normalize is true, normalization will be done explicit, else implicit</param>
	void propagate(double dt, bool forward, bool normalize = true);
};

