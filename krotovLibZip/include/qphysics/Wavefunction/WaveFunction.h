#pragma once
#include <armadillo>

class HamiltonianSystem;

/// <summary>
/// This is the core class for representing and 
/// manipulating a quantum wavefunction.
/// </summary>
class WaveFunction
{
protected:
	/// <summary>
	/// the state is a list of complex numbers which are the mathematical
	/// description of the wave function.
	/// </summary>
	arma::cx_vec state;

public:
	HamiltonianSystem& H;

	/// <summary>
	/// this creates wavefunction obj with empty state.
	/// using this contructor requires you to manually set the state.
	/// </summary>
	WaveFunction(HamiltonianSystem& H);

	WaveFunction(arma::cx_vec state, HamiltonianSystem& H);
	WaveFunction(arma::vec state, HamiltonianSystem& H);

	//WaveFunction();

	/// <summary>
	/// Copy constructor.
	/// </summary>
	/// <param name="wave"></param>
	WaveFunction(const WaveFunction &wave);

	/// <summary>
	/// Returns a reference of the wavefunction's state.
	/// Only use this function if you wish to manipulate the state
	/// outside of the qphysics.
	/// </summary>
	arma::cx_vec returnState();

	arma::cx_vec& returnStateRef(){
		return state;
	}

	virtual ~WaveFunction();
	void initFFTW();

	/// <summary>
	/// This method ensures the wavefunction is normalized. 
	/// This means the wavefunction is a probability distribution.
	/// This method should be called after using the operator functions.
	/// </summary>
	void normalize();

	/// <summary>
	/// Changes the state for the wavefunction.
	/// </summary>
	/// <param name="newState">New state.</param>
	void setState(arma::cx_vec state);

	/// <summary>
	/// Changes the state for the wavefunction.
	/// </summary>
	/// <param name="newState">New state.</param>
	void setState(arma::vec state);

	/// <summary>
	/// This returns the probablity distribution for the 
	/// location of an atom.
	/// 
	/// This is the "quantum liquid" displayed in the game.
	/// </summary>
	/// <returns>The abs squared state Vector of the wavefunction.</returns>
	arma::vec absSquare();

	void scaleState(std::complex<double> s);

	/// <summary>
	/// This calculates the quantum overlap between two wavefunctions (<chi|psi>).
	/// This overlap is not to be confused with a graphical overlap.
	/// </summary>
	/// <returns>The overlap.</returns>
	/// <param name="wavefunction">Wavefunction.</param>
	std::complex<double> getOverlap(WaveFunction wavefunction);

	/// <summary>
	/// This calculates the fidelity between two wavefunctions.
	/// The fidelity is a measure of similarity between two wavefunctions.
	/// </summary>
	/// <returns>The fidelity of two wavefunctions.</returns>
	/// <param name="wavefunction">Wavefunction on which to compare the fidelity.</param>
	double getFidelity(WaveFunction wavefunction);

	// OPERATORS
	// These operators define a convenient way to calculate a superpositions of wavefunctions.
	// Remember to normalize the state after using these operators!

	/// sum operator (wavefunction + wavefunction)
	WaveFunction operator+(const WaveFunction& waveFunction);
	/// subtract operator (wavefunction - wavefunction)
	WaveFunction operator-(const WaveFunction& waveFunction);
	/// multiply operator with double (wavefunction * double)
	WaveFunction operator*(const double& multiply);
	/// multiply operator with double (wavefunction * double)
	friend WaveFunction operator*(const double& multiply, const WaveFunction& waveFunction);
	/// multiply operator with complex double (complex double * wavefunction)	
	WaveFunction operator*(const std::complex<double>& multiply);
	friend WaveFunction operator*(const std::complex<double>& multiply, const WaveFunction& waveFunction);
	/// divide operator with double (wavefunction / double)
	WaveFunction operator/(const double& divider);
	/// divide operator with complex double (wavefunction / complex double)
	WaveFunction operator/(const std::complex<double>& multiply);
	//equals operator (wavefunction = wavefunction) #### Use setState() instead
	WaveFunction operator=(const WaveFunction& waveFunction);
};


