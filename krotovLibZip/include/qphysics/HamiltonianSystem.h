#pragma once
#include <armadillo>
#include <mutex>
#include "Potentials/Potential.h"

class DynamicWaveFunction;

static std::mutex m;

enum PotentialType{
	STATIC,
	DYNAMIC,
	BOTH
};

/// <summary>
/// Hamiltonian system contains the matrix representation 
/// of the hamiltonian operator for problem. The hamiltonian describes the 
/// kinetic and potential energy of a quantum system.
/// Hamiltonian system has methods for diagonalization,
/// which obtain excited states and ground states.
/// </summary>
class HamiltonianSystem
{
private:
	arma::vec discreteDynamicPotential;
	arma::vec discreteStaticPotential;
	
	Point currentPoint;

	double absolutTime = 0.0;

	void constructMomentumArray(double xMinVal, double xMaxVal);
	void constructFiveDiagKineticHamiltonian();

	void scaleAndNormalizeEigenVecs(arma::mat& eigvecs);

public:
	HamiltonianSystem(PotentialPtr staticPotential, PotentialPtr dynamicPotential, double kinematicFactor, int nSpatialPts, double xMinVal, double xMaxVal);
	virtual ~HamiltonianSystem();

	const double kinematicFactor;
	const unsigned int nSpatialPts;
	double dx; //grid spacing

	arma::vec xArray;
	arma::vec kArray;
    
    PotentialPtr dynamicPotential;

	/// <summary>
	/// Updates the extremum of potential and increases the internal time. When using this in conjunction with propagateWavefunction(...),
	/// make sure that the timesteps are consistent.
	/// </summary>
	/// <param name="p">A point containing the new potential extremum and increase in time</param>
    /// <param name="forwardsTime">Bool to indicate in which direction absolute time has to be updated.
    /// Should be based on whether the wave is propagated forward(true) or backwards(false) in time.</param>
	void updatePotential(Point p, bool forwardsTime = true);

	/// <summary>
	/// The current extremum of the potential
	/// </summary>
	/// <returns>The current point of the potential</returns>
	Point getCurrentPoint();

	/// <summary>
	/// The absolute time maintained by the HamiltonianSystem
	/// </summary>
	/// <returns>The absolute time</returns>
	double getAbsoluteTime()
	{
		return absolutTime;
	}

	/// <summary>
	/// Resets the internal time to zero.
	/// </summary>
	void resetTime();

	// implement proper resetHamiltonian function for backward propogation
	void backwardsPropogationPointReset(Point point){
		currentPoint = point;
	}

	/// <summary>
	/// Return copy of the xArray.
	/// </summary>
	/// <returns>The X array.</returns>
	arma::vec getXArray();

	/// <summary>
	/// Return copy of the kArray.
	/// </summary>
	/// <returns>The K array.</returns>
	arma::vec getKArray();

	/// <summary>
	/// Returns copy of the total potential.
	/// </summary>
	/// <returns>The potential.</returns>
	arma::vec getDiscretePotential();

	/// <summary>
	/// Calculates the states of the ground and excited.
	/// </summary>
	/// <param name="x">state reference.</param>
	void calculateGroundAndExcitedState(arma::cx_vec& x);

	arma::mat getHamiltonMatrix();
	/// <summary>
	/// This method diagonalizes the hamiltonian using the internal potential and returns
	/// the lowest (stateCount + 1) states and eigen values. 
	/// StateCount == 0 returns ground state.
	/// StateCount == 1 returns ground + 1st excited states.
	/// And etc.
	/// To specify the potential in the diagonalization use the three public static integers specified in this class, DYNAMIC, STATIC or BOTH.
	/// </summary>
	/// <param name="eigvecs">Eigen vectors matrix as out reference.</param>
	/// <param name="eigenVals">Eigen values vector as out reference.</param>
	/// <param name="stateCount">State count.</param>
	/// <param name="pot">The internal potential used in the diagonlization</param>
	void diagonalizer(arma::mat& eigvecs, arma::vec& eigenVals, int stateCount = 0, PotentialType potType = BOTH);

	/// <summary>
	/// This method diagonalizes the hamiltonian using a arbitrary Potential and returns
	/// the lowest (stateCount + 1) states and eigen values. 
	/// StateCount == 0 returns ground state.
	/// StateCount == 1 returns ground + 1st excited states.
	/// And etc.
	/// </summary>
	/// <param name="eigvecs">Eigen vectors matrix as out reference.</param>
	/// <param name="eigenVals">Eigen values vector as out reference.</param>
	/// <param name="potential">The Potential used in the diagonlization</param>
	/// <param name="stateCount">State count.</param>
	void diagonalizer(arma::mat& eigvecs, arma::vec& eigenVals, Potential& potential, int stateCount = 0);

protected:
	arma::mat kineticEnergy;

	void diagonalizeOnFiveDiag(arma::mat& eigvecs, arma::vec& eigenVals, arma::vec pot, int stateCount = 0);
};

