#pragma once
#include "TargetArea.h"

class AllTargetArea :
	public TargetArea
{
public:
	/// <summary>
	/// A target area that is everything, i.e. spans double.minValue to double.MaxValue on both axes.
	/// </summary>
	AllTargetArea();
	~AllTargetArea();
};
