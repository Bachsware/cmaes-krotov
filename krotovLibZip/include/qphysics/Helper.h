//
//  Helper.h
//  qphysics
//
//  Created by Donatas Tutinas on 08/04/15.
//  Copyright (c) 2015 Donatas Tutinas. All rights reserved.
//

#ifndef __qphysics__Helper__
#define __qphysics__Helper__

#include <stdio.h>
#include "Types.h"
#include "Wavefunction/DynamicWaveFunction.h"
#include "Level.h"
#include <sys/stat.h>

/* Place for common services and functions
 *
 */
namespace Helper {
	Level* initLevel(const std::string& jsonLvlPath, bool twiceSpatPts = false);

	//adds fidelities along the given path
	void addFidelitiesAlongThePath(std::vector<MatPoint>& path, Level& level, DynamicWaveFunction& dWave);

    //check if a given file exists
    bool fileExists (const std::string& name);
    
    // replace string parts with given argument
    std::string ReplaceAll(std::string str, const std::string& from, const std::string& to);
    
    //split string on a given char to a given vector
    std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
    
    //split string on a given char
    std::vector<std::string> split(const std::string &s, char delim);
    
	/// external interface for getting fidelities along the given path
	extern "C" double getFidelitiesAlongThePath(double* x, double* amp, double* dTime, double* fidelity, int size, const char* levelFilePath);

	///external interface to optimize a given path. Also attaches
	///fidelities along the optimized path
	extern "C" double optimizePath(double* x, double* amp, double* dTime, double* fidelity, int size, const char* levelFilePath);
    
    double getFidelitiesAlongThePath(std::vector<double> x, std::vector<double> amp, std::vector<double> dTime, std::vector<double>& fidelities, const std::string& levelFilePath);
    double optimizePath(std::vector<double> x, std::vector<double> amp, std::vector<double> dTime, std::vector<double>& fidelities, const std::string& levelFilePath);
    
    //exception type for dealing with bad json files
    class BadLevel : public std::exception {
    private:
        std::string message = "Failed to read the json level file!";
        
    public:
        BadLevel() {}
        BadLevel(std::string message) : message(message)
        {
            
        }
        virtual const char* what() const throw()
        {
            return message.c_str();
        }
    };
    
    class NotImplementedException
    : public std::exception {
        
    public:  
        // Construct with given error message:
        NotImplementedException(const char * error = "Functionality not yet implemented!")
        {
            errorMessage = error;
        }
        
        // Provided for compatibility with std::exception.
		const char * what() const throw()
        {
            return errorMessage.c_str();
        }
        
    private:
        std::string errorMessage;
    };
}

#endif /* defined(__qphysics__Helper__) */
