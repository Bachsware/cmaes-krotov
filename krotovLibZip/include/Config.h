#pragma once
#include <string>
#include "krotov/Krotov.h"

class Config
{
private:
	//default cfg file name
	std::string filename = "krotovParam.conf"; //default config filename

	//checks if method starts with given target str
	bool startsWith(std::string str, std::string target);

	//methods to convert str to needed type
	//can throw exception if failed
	int toInt(std::string str);
	double toDouble(std::string str);
	bool toBool(std::string str);
	Point toPoint(std::string str);
	//end convert methods

	//splits string on whitespace
	std::vector<std::string> split(std::string const &input);

	KrotovParam kParam;
	const int minThreads = 1; //minimum allowed threads count
	const int minBufferSize = 1;

public:
	Config();
	~Config();

	//returns string with symbols converted to non capital values
	static void toLowCase(std::string& str);

	//reads config values from the set filename.
	//values are stored inside Krotov param variable.
	void readConfig();

	//sets given filename as config file
	void setConfigFile(const std::string filename);

	//returns Krotov parameter value
	//if none was read from file, it will contain
	//default values
	KrotovParam getKrotovParam();
};

