#pragma once
#include <armadillo>
#include "../qphysics/Level.h"
#include "../qphysics/Wavefunction/DynamicWaveFunction.h"

//krotov settings struct
struct KrotovParam {
	//performance settings
	int threadsCount = 5;
	int bufferSize = 1000;

	//krotov algorithm settings
    bool iterationPaths = false;
	bool twiceSpatPts = false;
	double sufficientFidelity = 0.999; //default 0.999
	unsigned int maxIterations = 800;
	bool allowTargetChange = true;
	bool allowStartChange = false;
	int directionMode = 2;
	Point stepScale = { 1e-5, 1e-5 };
	//Point maxDudt = { 2 / 0.002, 250 / 0.002 };
	double rGoal = 0.4;
	Point max = { 1e-4, 1e-4 };
	Point min = { 1e-20, 1e-20 };
	int fidelityBreak = 100; //default 100, change to krotov iterations for maximum accuracy
	double fidelityBreakVal = 1e-7; //default 1e-5

	//for krotov sweep
	int finalPoints = 200;
	double finalDuration = 0.068;
	double finalDeltaDuration = 0.002;
	double middleDuration = 0.4;
	double middleDeltaDuration = 0.008;

	bool fidelitiesAlongThePath = true;
};

class Krotov{
private:
	DynamicWaveFunction& psiFunction;
	DynamicWaveFunction& chiFunction;

	//path to optimize
	std::vector<PathPoint>& startGuess;

	//krotov work variables
	unsigned int lentU = 2;
	unsigned int uStart, endpoint, forwardstop, backwardstop, lenT;

	KrotovParam krotovParam;

	//inits length values etc
	void init(int guessLength, bool onlyX);

	void loop(unsigned int uStart, arma::mat& gradH, unsigned int n, arma::cx_mat& chis, arma::cx_mat& psis);
	inline void uchecker(Point& uval, RefPoint& gradH, Point prevPoint, unsigned int n, unsigned int j);

public:
	Level& level;
	Krotov(Level& level, std::vector<PathPoint>& startGuess, KrotovParam krotovParam, DynamicWaveFunction& psiFunction, DynamicWaveFunction& chiFunction);
	std::vector<double> krotov(bool onlyX = false);
	void adjustStepScale(const arma::mat& previousGradH, const arma::mat& gradH);
	void initPsis(arma::cx_mat& psis, std::vector<PathPoint>& startGuess);
	void initChis(arma::cx_mat& chis, std::vector<PathPoint>& startGuess, arma::cx_mat& psis, WaveFunction& targetState);
	KrotovParam& getKrotovParam(){
		return krotovParam;
	}
	void setKrotovParam(KrotovParam kParam){
		this->krotovParam = kParam;
	}
    std::vector<Path> iterationPaths;
};
