/*
Krotov Sweep functions
*/

#pragma once
#include "Krotov.h"

namespace KrotovSweep {
	namespace subfunctions {
		std::vector<MatPoint> squeezePath(std::vector<MatPoint>& path, double newTime);
		void timeToAbsoluteRepresentation(std::vector<MatPoint>& path);
		int binarySearch(std::vector<MatPoint>& path, double z);
		MatPoint pointInterpolation(std::vector<MatPoint>& path, double newTime);
		std::vector<MatPoint> truncatePath(std::vector<MatPoint>& path, int newPoints, double duration);
		void doKrotov(Level& level, Path& path, Hit& hit, KrotovParam kParam, DynamicWaveFunction& psiF, DynamicWaveFunction& chiF);
	}

	bool truncateSweep(Level& level, Hit& hit, KrotovParam krotovParam, DynamicWaveFunction& psiF, DynamicWaveFunction& chiF, bool singleSweep = false);
}