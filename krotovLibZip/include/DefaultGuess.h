#pragma once
#include "qphysics/Types.h"
#include "qphysics/Level.h"

namespace DefaultGuess
{
	std::vector<PathPoint> bringHomeWater(Level& level, double t, double deltaT);
	std::vector<PathPoint> bringHomeWaterDynamicDeltaTime(Level& level, double t, double deltaT);
	std::vector<PathPoint> harmonic(Level& level, double t, double deltaT);
};

