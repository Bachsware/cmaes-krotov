//============================================================================
// Name        : ginac.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Helper.h"
#include "DefaultGuess.h"

using namespace std;

int main()
{
	std::string jsonLvlPath = "bringhomewater.json";

	//init level
	Level* level = Helper::initLevel(jsonLvlPath, false);

	//get some path
	std::vector<MatPoint> path = DefaultGuess::bringHomeWater(*level, 0.6, 0.002);

	//convert path to multiple vectors
	std::vector<double> x, amp, dTime, fidelities;
	for (int i = 0; i < path.size(); i++)
	{
		x.push_back(path[i].x);
		amp.push_back(path[i].amp);
		dTime.push_back(path[i].dTime);
	}

	//run optimization and print result
	double res = Helper::optimizePath(x, amp, dTime, fidelities, jsonLvlPath);
	std::cout << res << std::endl;
	return 0;
}
