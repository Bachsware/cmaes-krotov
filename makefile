LIBS := -lkrotovLib -larmadillo -lsymbolicGCC5lib -lpthread
DEPS := mainSpline.o PathBuilder.o Util.o Hit.o Cmaes.o PieceWiseFit.o
PATHS:= -I./krotovLibZip/include -I./krotovLibZip/deps

# All Target
all: engine.txt QphysicsSpline
	./Qphysics
# Tool invocations
engine.txt: qPhysics.zip
	unzip $<
	@echo 'success' > $@
QphysicsSpline: ${DEPS}
	@echo '|---| Makefile: Qphysics |---------------------------------|'
	@echo '   Building target: $@'
	@echo '   Invoking: GCC C++ Linker'
	g++  -std=c++1y -L./krotovLibZip/ -LqPhysics -o "Qphysics" $^ $(LIBS)
	@echo ''
	@echo '   Finished building target --> $@'
	@echo ''

mainSpline.o : ./src/mainSpline.cpp
	@echo '|---| Makefile: main.o |-----------------------------------|'
	g++ -std=c++1y $(PATHS) -O3 -g3 -march=native -c -fmessage-length=0 -MMD -MP -MF mainSpline.d -MT mainSpline.o -o mainSpline.o $<
	@echo ''

PieceWiseFit.o : ./src/PieceWiseFit.cpp
	@echo '|---| Makefile: PieceWiseFit.o |---------------------------|'
	g++ -std=c++1y -c $<
	@echo ''

PathBuilder.o : ./src/PathBuilder.cpp
	@echo '|---| Makefile: PathBuilder.o |----------------------------|'
	g++ -std=c++1y -c -O3 -g3 -march=native $<
	@echo ''

Util.o : ./src/Util.cpp
	@echo '|---| Makefile: Util.o |-----------------------------------|'
	g++ -std=c++1y -c $<
	@echo ''

Hit.o : ./src/Hit.cpp
	@echo '|---| Makefile: Hit.o |------------------------------------|'
	g++ -std=c++1y $(PATHS) -c $<
	@echo ''

Cmaes.o : ./src/Cmaes.cpp PieceWiseFit.o
	@echo '|---| Makefile: Cmaes.o |----------------------------------|'
	g++ -std=c++1y $(PATHS) -c -O3 -g3 -march=native  $<
	@echo ''

# Other Targets
Optimize : Qphysics
	./$<

cleanAll:
	-$(RM) -r Data
	mkdir Data
	-$(RM) Qphysics
	-$(RM) -r qPhysics
	-$(RM) *.o *.d *.gch *.txt
	-@echo 'makefile: targets and data cleaned'
	-@echo ' '

clean:
	-$(RM) -r Data
	mkdir Data
	-@echo 'makefile: data cleaned'
	-@echo ' '
