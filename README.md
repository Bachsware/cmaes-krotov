# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
   - Optimization tool for quantum optimal control theory, combining CMAES with Krotov optimization.

* Version 1.0

### How do I get set up? ###

* Summary of set up
   - Open Terminal --> make
* Configuration
   - To clean build: make cleanAll
   - To clean datafiles: make clean
* Dependencies
   armadillo
   unitstd
   pthread
   symbolicGCC5
   You will need KrotovLib: https://drive.google.com/open?id=0BzskTcFw8mqhVEhZekhRWEszM0k (extract to usr/local)

### Who do I talk to? ###

* Repo owner or admin
   - Christian Bach
   - christiankx450@gmail.com