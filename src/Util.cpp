#include "Util.h"

double Util::normVec(arma::vec v){
	double res = 0;
	for(int i=0; i<int(v.n_rows); i++){
		res += pow(v(i),2);
	}
	return sqrt(res);
}
double Util::MIN(double a, double b){
    if (a<b) return a;
    return b;
}
double Util::MAX(double a, double b){
    if (a>b) return a;
    return b;
}
