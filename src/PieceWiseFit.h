#include <iostream>
#include <armadillo>

//arma::vec fitPiecewiseLinearFunction(arma::vec& x,arma::vec& y, arma::vec& xPositions,arma::vec& lowerbounds, arma::vec& upperbounds){
std::vector<double> fitPiecewiseLinearFunction(std::vector<double>& x_in,std::vector<double>& y_in, std::vector<double>& xPositions_in,std::vector<double>& lowerbounds_in, std::vector<double>& upperbounds_in);
