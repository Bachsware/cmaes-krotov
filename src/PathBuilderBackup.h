#include <armadillo>
#include <iostream>

using namespace arma;

class PathBuilder{
public:
  arma::vec CRABPath(double xi, double xf, double scale, arma::vec c, double duration);
  arma::vec splinePath(double xi, double xf, double scale, arma::vec x, int N);
};
