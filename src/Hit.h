#include <armadillo>
#include <iostream>
#include "DefaultGuess.h"
#include "Helper.h"

using namespace arma;
using namespace std;

class Hit{
private:
  double xi,xm,xf,ai,af;
  vector<double> x,a,dt,f;
  double duration;
  arma::vec coeffs;
  string jsonLvlPath;
  double sigmaStart;
  double targetF;
  int ID;
public:
  // Mutators
  void setX(std::vector<double> X);
  void setA(std::vector<double> X);
  void setDt(std::vector<double> X);
  void setF(std::vector<double> X);

  void setLevel(string X);
  void setSigma(double X);
  void setTarget(double X);
  void setCoeffs(arma::vec X);

  // Acessors
  double getDuration();
  int getID();

  vector<double> getX();
  vector<double> getA();
  vector<double> getDt();
  vector<double> getF();

  double getXi();
  double getXm();
  double getXf();
  double getAi();
  double getAf();

  string getLevel();
  double getSigma();
  double getTarget();
  arma::vec getCoeffs();

  Hit(double xI, double xm, double xF, double aI, double aF, double Duration, arma::vec Coeffs, int id);
};
