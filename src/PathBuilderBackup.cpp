#include "PathBuilder.h"
/*
  This function creates path form a vector containing amplitudes to make a harmonic pertubation
  Function needs duration which is a double = no. of grid points in hte x-map

  Returns a vector x(t) values
*/
arma::vec PathBuilder::CRABPath(double xi, double xf, double scale, arma::vec c,double duration){
  // highest wave number in pertubation
	int f_max = c.n_rows/2;
	// params for the straight line

  // Converting input vector to two different ones
	vec a = vec(f_max);
	vec b = vec(f_max);
	for(int i = 0; i<f_max; i++){
			a(i) = c(i);
			b(i)=c(i+f_max);
	}

	arma::vec path = arma::vec(duration,fill::ones);
	path = xi*path;

	// Create path <-- Just a straight line from X_i to X_f in [0,duration]
	for(int i=0; i<duration; i++){
		path(i)+=(xf-xi)/(duration-1) * i;
	}

	//Create Harmonic pertubation
	arma::vec pertubation = arma::vec(duration,fill::zeros);

	double pi = datum::pi;
	for(int  j = 1; j<f_max; j++){
		for(int i=0; i<duration; i++){ // add sine and cos to pertubation
			pertubation(i) += a(j)*cos(2*pi*(j+1/2)*i/duration) + b(j)*sin(2*pi*j*i/duration);
		}
	}
  // Control the amplitudes
	pertubation = pertubation/max(abs(pertubation))*scale;

  // This is damping for the whole pertubation as sines accumulate a rough bump otherwise
	for(int i=0; i<duration; i++){
		pertubation(i)*=exp(-1*pow( (i-duration*5/6) /(duration*0.6),6));
	}
	return path+pertubation;
}

arma::vec PathBuilder::splinePath(double xi, double xf, double scale, arma::vec x, int N){
  // Linear interpolation
  int n = x.n_rows;
	vec t = arma::linspace(0,1,n);
	vec T = arma::linspace(0,1,N);
  vec Y = arma::linspace(xi,xf,N);
  vec P = arma::linspace(0,0,N);

	double x0,x1,t0,t1;
	int rightIndex = 1;

	for (size_t i = 0; i < N; i++) {
		if(t(rightIndex) < T(i) ){
			rightIndex++;
		}
		x0 = x(rightIndex);
		x1 = x(rightIndex-1);
		t0 = t(rightIndex);
		t1 = t(rightIndex-1);

		P(i)= x0 + (x1-x0)/(t1-t0) * (T(i)-t0);
	}
	P(0)=0;

  return Y+P/max(abs(P))*scale;
}
