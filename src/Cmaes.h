#include <armadillo>
#include <iostream>
#include <fstream>
#include "Hit.h"
#include "PathBuilder.h"
#include "Util.h"
#include "Helper.h"

using namespace std;
using namespace arma;
class Cmaes{
private:
  double * indexPointer;
  Util util;
  PathBuilder pathBuilder;
  double f(std::vector<double>X,
    std::vector<double>amp,
    std::vector<double>dt,std::vector<double>F,
    string lvl
  );
public:
  double optimize(Hit h);
  Cmaes(double * IndexPointer);
  arma::vec getXCoeffs(arma::vec C);
  arma::vec getACoeffs(arma::vec C);
};
