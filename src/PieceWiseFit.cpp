#include <iostream>
#include <armadillo>

//arma::vec fitPiecewiseLinearFunction(arma::vec& x,arma::vec& y, arma::vec& xPositions,arma::vec& lowerbounds, arma::vec& upperbounds){
    std::vector<double> fitPiecewiseLinearFunction(std::vector<double>& x_in,std::vector<double>& y_in, std::vector<double>& xPositions_in,std::vector<double>& lowerbounds_in, std::vector<double>& upperbounds_in){
    arma::vec x(x_in); arma::vec y(y_in); arma::vec xPositions(xPositions_in); arma::vec lowerbounds(lowerbounds_in); arma::vec upperbounds(upperbounds_in);
    // Start piecewise fit algorithm: See http://golovchenko.org/pwl_fit/index.htm for details
    double eps = (x(1)-x(0))/10;
    arma::mat A; arma::vec B;

    unsigned int numberOfParams = xPositions.size();
    A.zeros(numberOfParams,numberOfParams); B.zeros(numberOfParams);

    arma::ivec js(xPositions.size());
    js.zeros();
    unsigned int jIndex = 0;
    for(unsigned int i=0;i<x.size();i++){
        if(x[i]>xPositions[jIndex]+eps && x[i+1]>=xPositions[jIndex]-eps){
            js[jIndex]=i;
            jIndex++;
        }
    }
    if(xPositions[numberOfParams-1]==x(x.size()-1)){
       js[js.size()-1]=x.size();
     }
    double* x_ptr = x.memptr();
    double* y_ptr = y.memptr();

    for(unsigned int i=0;i<numberOfParams;i++){
        if(i!=0){
            arma::vec xSlice(x_ptr+js[i-1],js[i]-js[i-1],true,true);
            arma::vec ySlice(y_ptr+js[i-1],js[i]-js[i-1],true,true);
            double denominator = xPositions[i]-xPositions[i-1];
            B[i] = B[i] + (arma::sum(xSlice%ySlice)-xPositions[i-1]*arma::sum(ySlice))/denominator;;
            denominator = denominator * denominator;
            A.at(i,i-1) -= arma::sum((xSlice-xPositions[i-1])%(xSlice-xPositions[i]))/denominator;
            A.at(i,i) += arma::sum(arma::square(xSlice-xPositions[i-1]))/denominator;
        }
        if(i!=numberOfParams-1){
            arma::vec xSlice(x_ptr+js[i],js[i+1]-js[i],true,true);
            arma::vec ySlice(y_ptr+js[i],js[i+1]-js[i],true,true);
            double denominator = xPositions[i+1]-xPositions[i];
            B[i] = B[i] + (-arma::sum(xSlice%ySlice)+xPositions[i+1]*arma::sum(ySlice))/denominator;
            denominator = denominator*denominator;
            A.at(i,i) = A.at(i,i) + arma::sum(arma::square(xSlice-xPositions[i+1]))/denominator;
            A.at(i,i+1) = A.at(i,i+1) - arma::sum((xSlice-xPositions[i])%(xSlice-xPositions[i+1]))/denominator;
        }
    }
    arma::vec result =arma::solve(A,B);
    for(unsigned int i=0;i<numberOfParams;i++) {
        if(result[i] < lowerbounds[i]) {result[i] = lowerbounds[i]; }
        if(result[i] > upperbounds[i]) {result[i] = upperbounds[i];}
    }
    std::vector<double> result_out = arma::conv_to<std::vector<double>>::from(result);
    return result_out;
}
