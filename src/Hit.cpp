#include "Hit.h"

// constructor
Hit::Hit(double xI, double xM, double xF, double aI, double aF, double Duration, arma::vec Coeffs, int id){ // Simple constructor --> default path from Helper (shit)
  xi = xI;  xm = xM; xf = xF;
  ai = aI;  af = aF;

  duration = Duration;
  ID = id;
  coeffs = Coeffs;
  targetF = 0.999;
  sigmaStart = 1;
  //init level
    jsonLvlPath = "bringhomewater.json";
    Level* level = Helper::initLevel(jsonLvlPath, false);

  //get some path
    double dT = duration/200;
    std::vector<PathPoint> path = DefaultGuess::bringHomeWater(*level, duration, dT);
    int N =int(path.size());

    int peakIndex =2;
    arma::vec start = arma::linspace(xi,xm,peakIndex); // steep ascend
    arma::vec end = arma::linspace(xm,xf,floor(N-peakIndex));
    for (int i = 0; i < N; i++)
    {
      if(i<peakIndex){
        x.push_back(start(i));
      } else{
        x.push_back(end(i-peakIndex));
      }
      a.push_back(path[i].amp);
	    dt.push_back(path[i].dTime);
    }

}

// Mutators
  void Hit::setX(std::vector<double> X) {x    = X;}
  void Hit::setA(std::vector<double> X) {a    = X;}
  void Hit::setDt(std::vector<double> X){dt   = X;}
  void Hit::setF(std::vector<double> X) {f    = X;}
  void Hit::setLevel(string X){jsonLvlPath    = X;}
  void Hit::setSigma(double X){sigmaStart     = X;}
  void Hit::setTarget(double X){targetF       = X;}
  void Hit::setCoeffs(arma::vec X){coeffs     = X;}

// Acessors
  vector<double> Hit::getX() {return x;}
  vector<double> Hit::getA() {return a;}
  vector<double> Hit::getDt(){return dt;}
  vector<double> Hit::getF() {return f;}

  double Hit::getXi(){return xi;}
  double Hit::getXf(){return xf;}
  double Hit::getAi(){return ai;}
  double Hit::getAf(){return af;}

  string Hit::getLevel(){return jsonLvlPath;}
  double Hit::getSigma(){return sigmaStart;}
  double Hit::getTarget(){return targetF;}
  arma::vec Hit::getCoeffs(){return coeffs;}
  double Hit::getDuration(){ return duration;}
  int Hit::getID(){return ID;}
