// Includes
#include <iostream>
#include <armadillo>
#include <thread>
#include "Cmaes.h"
#include <pthread.h>
#include <unistd.h>

// Namespaces
using namespace std;
using namespace arma;

// Field variables
long int N;
double sigmaStart = 1;
double F 			    = 0.90;
double Index      = 0;

arma::vec coeffs;

// Multithread variables
int NUM_THREADS       = 23; // Number of CPU cores
int NUM_THREADS_ALIVE = 0;

// Tools
PathBuilder pathBuilder;
Util        util;
Cmaes       cmaes(&Index);

//Containers
struct thread_data{
   long int  id;
   int  tID;
   double duration;
};

// Thread function
void *optProb(void *threadarg){
  struct thread_data *optiData;
  optiData = (struct thread_data *) threadarg;
  int    tID        = optiData -> tID;
  cout << "\t [Thread: "<< tID+1 << "]: Loading seed." << endl;

  long int    id    = optiData -> id;
  double duration   = optiData -> duration;

  double xi,xm,xf,ai,af;

  xi  = -0.8;
  xm = 0.6;
  xf  = xi;
  ai  = -100;
  af  = ai;
  int f_max = coeffs.n_rows/2;
  coeffs(0)=xi;

  cout << "\t [Thread: "<< tID+1 << "]: Executing job:\t"<< id+1<< " of " << N << endl;
  //id += 1631;
  Hit h(xi,xm,xf,ai,af,duration,coeffs,id);

  double Cres = cmaes.optimize(h);

  cout << "\t [Thread: "<< tID+1 <<"]: Finished job:\t"<< id+1<<"\t -->" << "  F: " <<Cres <<", dur: "<<duration<<", Dim: " << coeffs.n_rows << endl;
  NUM_THREADS_ALIVE--; // Sink flag
  pthread_exit(NULL);
}

void cutOffFrequenciesAbove(int F_newMax);

// Main method
int main(int argc, char *argv[]){
	cout << "Qphysics: Bachmode 1: " << endl;

	// Creating Seeds and optimization
		// Initial params
  		double	dD			   = 0.003; // decrement for duration in optimization
      double dur         = 0.162; //0.075; //0.082-dD*15;	  // Start duration
      int 	f_max 		   = 30; 	  // Highest allowed frequency for pertubation (must be even)
	    int 	f_min		     = 28;	  // Lowest allowed frequency
      int   F_decrement  = 2;     // Decrement size for f_max
      int   N_equalSeeds = 33;    // Number of equal seeds
      int   N_durations  = 51;    // Number of decrements of duration
      int   N_F_decrease = floor((f_max-f_min)/F_decrement);      // Number of decrements of duration

    // Initial coeffs vector
			arma::vec vX = (arma::vec(f_max,fill::randu)-0.5);
      arma_rng::set_seed_random();  // set the seed to a random value
      vX(0)=0;
      arma::vec vA = (arma::vec(f_max,fill::randu)-0.5);
      vA(0)=0;
      coeffs = arma::vec(2*f_max);
      arma_rng::set_seed_random();  // set the seed to a random value

      for (size_t i = 0; i < f_max; i++) {
        coeffs(i)=vX(i);
      }
      for (size_t i = 0; i < f_max; i++) {
        coeffs(f_max+i)=vA(i);
      }
		// Run the hits at different durations
    if(F_decrement==0){
      N = N_durations * N_equalSeeds;
    }
    else{
      N = N_durations * N_F_decrease * N_equalSeeds;
    }
    int rc;
    pthread_t threads[NUM_THREADS];
    int NN = (floor(N/NUM_THREADS)+1)*NUM_THREADS;
    struct thread_data thread[NN];
    unsigned int muS = 400;

    bool moreWork   = true;
    bool canDoMore  = true;

    int Fcounter = 0;
    int Dcounter = 0;
    int Ecounter = 0;

      cout << "Qphysics: Starting Threading" << endl;
    while(moreWork){
      canDoMore = 0 == NUM_THREADS_ALIVE; // Manual joining all cores
      if(canDoMore){
            // invoke threads
            cout << "Qphysics: continueing work" << endl;

            for (int tNUM = 0; tNUM < NUM_THREADS; tNUM++) {
              //Thread preparation
                long int i = Index;

                // Here we iterate counters in order to decrease duration and the size of the basis.
                if(Ecounter==N_equalSeeds){
                  Dcounter++;
                  Ecounter=0;
                  if(Dcounter==N_durations){
                      Fcounter++;
                      Dcounter=0;
                  }
                }
                Ecounter++;

                cout << "Qphysics: preparing seed" << endl;
                try{
                  cutOffFrequenciesAbove(f_max-Fcounter*F_decrement); // Decreasing size of basis
                  thread[i].id             = i;
                  thread[i].tID            = tNUM;
                  thread[i].duration       = dur-Dcounter*dD; // Decrease duration
                  Index++;
                  moreWork = N>Index;
                // Threading invoking;
                  if(N>Index-1){ // Dont work overtime
                    usleep(muS);
                    cout << "Qphysics: Passing seed to thread" << endl;
                    NUM_THREADS_ALIVE++; // Raise flag
                    rc = pthread_create(&threads[tNUM],NULL,optProb,(void *)&thread[i] );

                    if (rc){
                       cout << "Error:unable to create thread," << rc << endl;
                       exit(-1);
                    }
                  }
                }
                catch(std::logic_error & e){
                  cout << "i=" << i << endl;
                  cout << "tNUM=" << tNUM << endl;
                  cout << "Dcounter=" << Dcounter << endl;
                  cout << "Index=" << Index << endl;
                  cout << "Err here =" << i << endl;

                  cutOffFrequenciesAbove(f_max-Fcounter); // Decreasing size of basis
                  thread[i].id             = i;
                  thread[i].tID            = tNUM;
                  thread[i].duration       = dur-Dcounter*dD; // Decrease duration
                  Index++;
                  moreWork = N>Index;
                // Threading invoking;
                  if(N>Index-1){ // Dont work overtime
                    usleep(muS);
                    cout << "Qphysics: Passing seed to thread" << endl;
                    NUM_THREADS_ALIVE++; // Raise flag
                    rc = pthread_create(&threads[tNUM],NULL,optProb,(void *)&thread[i] );

                    if (rc){
                       cout << "Error:unable to create thread," << rc << endl;
                       exit(-1);
                    }
                  }
                }
            }
      }
      else{
        sleep(1);
      }
    }
  pthread_exit(NULL);
  return 0;
}

void cutOffFrequenciesAbove(int F_newMax){
	if(F_newMax*2 > coeffs.n_rows) cout << "cutOffFrequencues -> Error: trying to increase coeffs"         << endl ;
	if(F_newMax*2 <= 2)            cout << "cutOffFrequencues -> Error: cant decrease below 2 frequencies" << endl ;
	else{
		int dim 		=	coeffs.n_rows;

		arma::vec a = arma::vec(dim/2);
		arma::vec b = arma::vec(dim/2);

		for(int i = 0; i<dim/2; i++){
				a(i) 	  =	coeffs(i);
				b(i)	  =	coeffs(i+dim/2);
		}
		coeffs.clear();
		coeffs = arma::vec(F_newMax*2,fill::zeros);
		for(int i = 0; i<F_newMax; i++){
			coeffs(i)			= a(i);
			coeffs(i+F_newMax)	= b(i);
		}
	}
}
