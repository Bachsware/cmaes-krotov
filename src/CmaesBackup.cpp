#include "Cmaes.h"
#include "PieceWiseFit.h"
struct KrotovResult {
		std::vector<double> iterationFidelities;
		std::vector<Path> iterationPaths;
		double finalFidelity;
		std::vector<PathPoint> finalPath;
};
int fitCounter = 0;
Cmaes::Cmaes(double * IndexPointer){
	indexPointer = IndexPointer;
//	cout << "file index: " << *indexPointer << endl;
}
// SplitCoeffs
arma::vec Cmaes::getXCoeffs(arma::vec C){
	int f_max = C.n_rows/2;
	arma::vec a = arma::vec(f_max);
	for(int i = 0; i<f_max; i++){
			a(i) = C(i);
	}
	return a;
}
arma::vec Cmaes::getACoeffs(arma::vec C){
	int f_max = C.n_rows/2;
	arma::vec a = arma::vec(f_max);
	for(int i = 0; i<f_max; i++){
			a(i) = C(i+f_max);
	}
	return a;
}
arma::vec setCoeffs(arma::vec X,arma::vec A){
	int f_max = X.n_rows*2;
	arma::vec a = arma::vec(f_max);
	for(int i = 0; i<f_max/2; i++){
		a(i) = X(i);
		a(i+f_max/2) = A(i);
	}
	return a;
}
// Objective function
double Cmaes::f(std::vector<double>X,std::vector<double>A,std::vector<double>dt,std::vector<double>F,string lvl){
  return Helper::getFidelitiesAlongThePath(X,A,dt,F,lvl,false);
}

// OPTIMIZATION ALGORITHM
double Cmaes::optimize(Hit h){
    //------------ Initialization ----------------------//
	  //cout << "CMAES: Optimizing.." << endl;
    // User defined input parameters
			arma::vec origin = conv_to< arma::vec >::from(h.getX()); // x_path from input
	    arma::vec coeffs 		= h.getCoeffs();
			double Np	=origin.n_rows;            // Dimension of Path (low-level problem)
			double N	=coeffs.n_rows;             // Dimension of  pertubation k-space (high-level problem)
			arma::vec xmean = coeffs;           // First xmean is just the startguess: coeffs

	    vector<double> dt = h.getDt();
	    vector<double> F = h.getF();
	    string lvl = h.getLevel();
			double xi,xf,ai,af;

			xi = h.getXi();
			xf = h.getXf();
			ai = h.getAi();
			af = h.getAf();
			arma::vec aTpi = linspace(0,Np*dt[1],N/2);

			std::vector<double> T,Xubi,Xlbi,Aubi,Albi;
			for (size_t i = 0; i < Np; i++) {
				T.push_back((i+1)*dt[1]);
				Xlbi.push_back(-0.07);
				Xubi.push_back(0.7);
				Albi.push_back(-600);
				Aubi.push_back(-100);
			}
			std::vector<double> Tpi;
			for (size_t i = 0; i < N/2; i++) {
				Tpi.push_back(aTpi(i));
			}

	    vector<double> amp = conv_to< std::vector<double> >::from(pathBuilder.splinePath(ai,af,200,getACoeffs(coeffs),Np));

			double sigma = h.getSigma();          // Initital step-size
			double stopfitness = h.getTarget();	  // When fitness is higher return solution

    // Strategy parameter setting: Selection
			int lambda = 4+floor(3*log(N));   	// Population size
			double mu = lambda/2;               // Population for succesfull offsprings

			double stopeval = 300*lambda;       // When exceeding stopeval evaluations of problem return solution

			arma::vec weights = arma::vec(int(mu)); // Weights for adapting
			double weightsSqrd=0;
			double weightsNotSqrd=0;
			for (int i = 0; i < int(mu); i++) {
				double res=log(mu+0.5)-log(double(i)+1);
				weights(i)=res;
				weightsSqrd+=pow(res,2);
				weightsNotSqrd+=res;
			}
			weights = weights / weightsNotSqrd;

			double mueff = pow(weightsNotSqrd,2)/weightsSqrd;

			mu= int(floor(mu)); // final construction of weights... From now on it does not chance

			double cc =  (4+mueff/N) / (N+4 + 2*mueff/N);
			double cs = (mueff+2) / (N+mueff+5);
			double c1 = 2 / (pow(N+1.3,2)+mueff);                                 // learning rate for rank 1 update
			double cmu = util.MIN(1-c1, 2 * (mueff-2+1/mueff) / (pow(N+2,2)+mueff));   // learning rate for rank mu update
			double damps = 1 + 2*util.MAX(0, sqrt((mueff-1)/(N+1))-1) + cs;            // Damping factor

    // Initialize dynamic (internal) strategy parameters and constants
			arma::vec pc = arma::vec(N,fill::zeros);
			arma::vec ps = pc;

    // For eigendecomposition of C
			mat B = mat(N,N,fill::eye);
			arma::vec D = arma::vec(N,fill::ones);
    // Covariance matrix C
			mat C = B * diagmat(D%D) * B.t();
			mat invsqrtC = B * diagmat(D/D/D) * B.t();

			double eigenval = 0;

			double chiN=pow(N,0.5)*(1-1/(4*N)+1/(21*pow(N,2))); // expectation of ||N(0,I)|| == norm(randn(N,1))

//----------------- Generation Loop ----------------------------------//

	cout << "CMAES: Optimizing.." << endl;

    try{
		double countEval = 0;
		// Preallocate matrix for k-dimensional
			mat arx = mat(N,lambda,fill::zeros);
			arma::vec arfitness = arma::vec(lambda);

			//cout << "CMAES: Optimizing.. Generation loop" << endl;
			std::vector<double> x;
			std::vector<double> amp;

		while (countEval< stopeval ){
			// Generate and evaluate lambda offspring
				for (int k = 0; k < int(lambda) ; k++) {
          // Fetch some stochastic sample from distribution
  					arma::vec NN = randn(N);
  					arma_rng::set_seed_random();  // set the seed to a random value
  					NN = NN/util.normVec(NN); // This is wierd but neccesary in order to converge
  					arma::vec v = xmean + sigma * B * (D % NN);


						v(0)=0; v(N/2)=0;
						std::vector<double> X = conv_to< std::vector<double> >::from(pathBuilder.splinePath(xi,xf,0.12,getXCoeffs(v),Np));
						std::vector<double> A = conv_to< std::vector<double> >::from(pathBuilder.splinePath(ai,af,100,getACoeffs(v),Np));
						A.at(0)=ai; X.at(0)=xi;

						double fit = 0;

						if (floor(fitCounter/(30*lambda)) >= 1) {
							// Go krotov
								Helper::KrotovResult krotovData = Helper::optimizePathExtra(X,A,dt,lvl);

								fit = krotovData.finalFidelity;
									std::vector<PathPoint> finalPath = krotovData.finalPath;
									for (size_t i = 0; i < Np; i++) {
										x.push_back(finalPath[i].x);
										amp.push_back(finalPath[i].amp);
									}

									cout << "\t\t\t\t\t\t\twe are good 2" << endl;
									arma::vec vX = conv_to< arma::vec >::from(fitPiecewiseLinearFunction(T,x,Tpi,Xlbi,Xubi));
									arma::vec vA = conv_to< arma::vec >::from(fitPiecewiseLinearFunction(T,amp,Tpi,Albi,Aubi));
									cout << "\t\t\t\t\t\t\twe are good 3" << endl;
									v = setCoeffs(vX,vA);
									fitCounter = 0;
						}
						else{
							// Normal CMA-ES procedure
							fit = f(X,A,dt,F,lvl); //Get fidelity from qphysics
							fitCounter++;
						}

						arx.col(k)=v;
						arfitness(k)=-fit; // Find target wave projection
						countEval++;

				}
			// Sorting best solutions
				uvec sorted = sort_index(arfitness);
				mat sortArx = mat(N,lambda);
				for (int i = 0; i < lambda; ++i) {
					sortArx.col(i)=arx.col(sorted(i));
				}
			// pertube old mean and save old one
				arma::vec xold = xmean;
				xmean = arma::vec(N,fill::zeros);
				for (int j = 0; j < int(mu); ++j) {
					xmean= xmean+sortArx.col(j)*weights(j);
				}

			// Cumulation: Update evolution paths
				ps = (1-cs)*ps + sqrt(cs*(2-cs)*mueff) * invsqrtC * (xmean-xold) / sigma;

				bool hsig = util.normVec(ps)/sqrt(1-pow(1-cs,2*countEval/lambda))/chiN < 1.4 + 2/(N+1);
				double trueFilter = 0;
				if(hsig) {
					trueFilter =1;
				}
				pc = (1-cc)*pc+ trueFilter * sqrt(cc*(2-cc)*mueff) * (xmean-xold) / sigma;

			// Adapt Covariance matrix C
				mat artmp = mat(N,int(mu),fill::zeros);
				for (int j = 0; j < int(mu); ++j) {
					artmp.col(j) = (1/sigma) * (sortArx(j)-xold);
				}

				C = (1-c1-cmu) * C + c1*(pc*pc.t()+ (1-trueFilter) * cc*(2-cc) * C) + cmu * artmp * diagmat(weights) * artmp.t();

			// Adapt step-size
				sigma = sigma * exp((cs/damps)*(norm(ps)/chiN - 1));

			// Decomposition of C into B*diag(D.^2)*B' (diagonalization)
				if (countEval - eigenval > lambda/(c1+cmu)/N/10) {  // to achieve O(N^2)
					eigenval = countEval;
					// enforce symmetry
						mat trimatu1 = trimatu(C)-diagmat(C);
						C = trimatu(C) + trimatu1;
					// [B, D] = eig(C); the matlab equivalent
						cx_vec eigval;
						cx_mat eigvec;
						eig_gen(eigval, eigvec, C);

						arma::vec D = real(eigval);
						mat B = real(eigvec); // They must be real values
						mat DD = mat(D.n_rows,D.n_rows,fill::zeros);

						for (int i=0; i<int(D.n_rows) ; i++){
							DD(i,i)=sqrt(D(i));
						}
					// Find the new invsqrt(C)
						mat DDD = diagmat(DD/DD/DD);
						invsqrtC = B * DDD * B.t();
				}
        cout << "\e[A"; //Line up in terminal
				cout << "   " << -arfitness(0)<<"\t"<< countEval << "\t" << sigma << endl; //Print status to terminal

				// Consider next move
				arx=sortArx;
				if( stopfitness <= -fitBest || countEval>stopeval){
					break;
				}
		}
//		cout << "CMAES: Done after: "<< countEval/lambda << " Generations"<< endl;
    arma::vec res = arx.col(0);

// --------------  Write .dat file with solution ---------------------------------//
		res(0)=0; res(N/2)=0;
		arma::vec Xpath = pathBuilder.splinePath(xi,xf,0.12,getXCoeffs(res),Np);
		arma::vec Apath = pathBuilder.splinePath(ai,af,100,getACoeffs(res),Np);
		Xpath(0)=xi;
		Apath(0)=ai;
		arma::vec AApath = h.getA();
		std::string fname = "./Data/resSpline";

		ostringstream convert;  // stream used for the conversion
		convert << h.getID();   // insert the textual representation of 'Number' in the characters in the stream
		fname += convert.str(); // set 'Result' to the contents of the stream
		fname+=".dat";
//		*indexPointer = *indexPointer + 1;
		ofstream file(fname);
	    file <<  -arfitness(0) << "   " << N << endl;
	    file << "#t\t X\t A"<< endl;
	    for(int i=0; i < int(Xpath.n_rows); i++){
		        file << i*dt[1] << "\t"<<  Xpath(i) << "\t" << Apath(i) << "\t" << amp[i] << endl;
		    }
		file.close();

		return -arfitness(0);
    }
    catch(std::logic_error & e){
      Cmaes::optimize(h);
    }
}
