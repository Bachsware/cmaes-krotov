#include <armadillo>
#include <iostream>

using namespace arma;

class PathBuilder{
public:
  arma::vec splinePath(arma::vec path, double scale, arma::vec x, int N);
};
