#include "PathBuilder.h"
/*
  This function creates path form a vector containing amplitudes to make a harmonic pertubation
  Function needs duration which is a double = no. of grid points in hte x-map
  Returns a vector x(t) values
*/
arma::vec PathBuilder::splinePath(arma::vec path, double scale, arma::vec x, int N){
  // Linear interpolation
  int n = x.n_rows;
	vec t = arma::linspace(0,1,n); // time space
	vec T = arma::linspace(0,1,N); // Spline points in time
  vec P = arma::linspace(0,0,N); // Pertubation

	double x0,x1,t0,t1;
	int rightIndex = 1;

	for (size_t i = 0; i < N; i++) {
		if(t(rightIndex) < T(i) ){
			rightIndex++;
		}
		x0 = x(rightIndex);
		x1 = x(rightIndex-1);
		t0 = t(rightIndex);
		t1 = t(rightIndex-1);

		P(i)= x0 + (x1-x0)/(t1-t0) * (T(i)-t0);
	}
	P(0)=0;
	if (max(P)>scale) {
		P=P/max(abs(P))*scale;
	}
  return path+P;
}
